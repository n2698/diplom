
output "vm-4_grafana" {
  value = yandex_compute_instance.vm-4.network_interface.0.nat_ip_address
}

output "vm-6_kibana" {
  value = yandex_compute_instance.vm-6.network_interface.0.nat_ip_address
}

output "vm-7_bastion" {
  value = yandex_compute_instance.vm-7.network_interface.0.nat_ip_address
}

output "balancer_ip" {
  #value = yandex_alb_load_balancer.test-balancer.listener[0].endpoint[0].address[0].external_ipv4_address
  value = yandex_alb_load_balancer.test-balancer.listener.0.endpoint.0.address.0.external_ipv4_address
}

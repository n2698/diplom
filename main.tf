# Provider settings
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}
provider "yandex" {
  token     = "LgAATuwfyIag8ucEy"
  cloud_id  = "cloud-pavlovvm00"
  folder_id = "ag4tg6auj2"
  zone      = "ru-central1-a"
}

# Create network
resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "network-1" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["10.2.0.0/24"]
}
resource "yandex_vpc_subnet" "network-2" {
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["10.3.0.0/24"]
}

resource "yandex_vpc_subnet" "network-3" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["10.4.0.0/24"]
}

resource "yandex_vpc_subnet" "network-4" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["10.66.0.0/24"]
}

resource "yandex_vpc_subnet" "privnet-1" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["10.14.0.0/24"]
  route_table_id = "${yandex_vpc_route_table.rt1.id}"
}

resource "yandex_vpc_subnet" "privnet-2" {
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["10.13.0.0/24"]
  route_table_id = "${yandex_vpc_route_table.rt2.id}"
}

resource "yandex_vpc_gateway" "nat-gateway" {
  name = "test-gateway"
  shared_egress_gateway {}
}

resource "yandex_vpc_route_table" "rt1" {
  name       = testrt
  network_id = yandex_vpc_network.network-1.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = "${yandex_vpc_gateway.nat-gateway.id}"
  }
}

resource "yandex_vpc_route_table" "rt2" {
  name       = testrt2
  network_id = yandex_vpc_network.network-1.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = "${yandex_vpc_gateway.nat-gateway.id}"
  }
}
#
#Create security group


resource "yandex_vpc_security_group" "public-group" {
  name        = "public-group"
  description = "Security group for public network"
  network_id  = "${yandex_vpc_network.network-1.id}"

  labels = {
    my-label = "pub-net"
  }

  ingress {
    protocol       = "TCP"
    description    = "grafana"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    port           = 3000
  }

  ingress {
    protocol       = "ANY"
    description    = "bastion"
    v4_cidr_blocks = ["10.66.0.0/24"]
    # from_port      = 1
    # to_port        = 35767
  }

  ingress {
    protocol       = "TCP"
    description    = "ssh"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    port           = 22
  }
  ingress {
    protocol       = "TCP"
    description    = "exporter"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
    port           = 9100
  }
  ingress {
    protocol       = "TCP"
    description    = "kibana"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    port           = 5601
  }
  ingress {
    protocol       = "ANY"
    description    = "in group"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    from_port      = 1
    to_port        = 35767
  }

  ingress {
    protocol       = "TCP"
    description    = "exporter"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
    port           = 4040
  }
  egress {
    protocol       = "TCP"
    description    = "prometheus"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    port      = 9090
  }
  ingress {
    protocol       = "TCP"
    description    = "prometheus"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
    port           = 9090
  }
  ingress {
    protocol       = "TCP"
    description    = "to web"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
    port           = 80
  }

  ingress {
    protocol       = "TCP"
    description    = "to web"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    port           = 80
  }


  egress {
    protocol       = "TCP"
    description    = "elastic"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    port      = 9200
  }
  egress {
    protocol       = "TCP"
    description    = "ssh"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    port      = 22
  }

  egress {
    protocol       = "ANY"
    description    = "en in gorup traffic"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    from_port      = 1
    to_port        = 35767
  }
  egress {
    protocol       = "ANY"
    description    = "bastion"
    v4_cidr_blocks = ["10.66.0.0/24"]
    # from_port      = 1
    # to_port        = 35767
  }
  egress {
    protocol       = "ANY"
    description    = "en in gorup traffic"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
  #   from_port      = 1
  #  to_port        = 35767 
  }

}


resource "yandex_vpc_security_group" "private-group" {
  name        = "private-group"
  description = "Security group for private network"
  network_id  = "${yandex_vpc_network.network-1.id}"

  labels = {
    my-label = "priv-net"
  }
 
  ingress {
    protocol       = "ANY"
    description    = "bastion"
    v4_cidr_blocks = ["10.66.0.0/24"]
    # from_port      = 1
    # to_port        = 35767
  }
   ingress {
    protocol       = "TCP"
    description    = "kibana2elastic"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    port           = 9200
   }
    ingress {
    protocol       = "TCP"
    description    = "grafana2prometheus"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    port           = 9090
    }

  ingress {
    protocol       = "TCP"
    description    = "rule1 description"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
    port           = 80
  }
  ingress {
    protocol       = "TCP"
    description    = "to web"
    v4_cidr_blocks = ["10.2.0.0/24", "10.3.0.0/24", "10.4.0.0/24"]
    port           = 80
  }
  ingress {
    protocol       = "TCP"
    description    = "exporter"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
    port           = 4040
  }
  ingress {
    protocol       = "TCP"
    description    = "exporter"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
    port           = 9100
  }
  ingress {
    protocol       = "TCP"
    description    = "prometheus"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
    port           = 9090
  }
  ingress {
    protocol       = "ANY"
    description    = "in group"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
    from_port      = 1
    to_port        = 35767
  }
  egress {
    protocol       = "ANY"
    description    = "en in gorup traffic"
    v4_cidr_blocks = ["10.13.0.0/24", "10.14.0.0/24"]
    # from_port      = 1
    # to_port        = 35767
  }
  egress {
    protocol       = "ANY"
    description    = "bastion"
    v4_cidr_blocks = ["10.66.0.0/24"]
    # from_port      = 1
    # to_port        = 35767
  }
}
#end create security group




# Create VM instance 1
resource "yandex_compute_instance" "vm-1" {
  name = "web1"
  zone = "ru-central1-a"
  resources {
    cores  = 2
    memory = 2
  }
  boot_disk {
    initialize_params {
      image_id = "fd8fte6bebi857ortlja"
      size = 5
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.privnet-1.id
    nat       = false
    security_group_ids = [yandex_vpc_security_group.private-group.id]
  }

  metadata = {
    #user-data = "${file("./userkey")}"
    ssh-keys = "ubuntu:${file("/home/kit/Learning/terraform/keys/ssh_key.pub")}"
  }
}

# Create VM instance 2
resource "yandex_compute_instance" "vm-2" {
  name = "web2"
  zone = "ru-central1-b"
  resources {
    cores  = 2
    memory = 2
  }
  boot_disk {
    initialize_params {
      image_id = "fd8fte6bebi857ortlja"
      size = 5
    }
  }
  network_interface { 
    subnet_id = yandex_vpc_subnet.privnet-2.id
    nat       = false
    security_group_ids = [yandex_vpc_security_group.private-group.id]
  }
  metadata = {
    #user-data = "${file("./userkey")}"
    ssh-keys = "ubuntu:${file("/home/kit/Learning/terraform/keys/ssh_key.pub")}"
  }
}

#Create Prometheus VM
resource "yandex_compute_instance" "vm-3" {
  name = "prometheus"
  zone = "ru-central1-b"
  resources {
    cores  = 2
    memory = 2
  }
  boot_disk {
    initialize_params {
      image_id = "fd8fte6bebi857ortlja"
      size = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.privnet-2.id
    nat       = false
    security_group_ids = [yandex_vpc_security_group.private-group.id]
  }
  metadata = {
    #user-data = "${file("./userkey")}"
    ssh-keys = "ubuntu:${file("/home/kit/Learning/terraform/keys/ssh_key.pub")}"
  }
}

#Create Grafana VM
resource "yandex_compute_instance" "vm-4" {
  name = "grafana"
  zone = "ru-central1-b"
  resources {
    cores  = 2
    memory = 2
  }
  boot_disk {
    initialize_params {
      image_id = "fd8fte6bebi857ortlja"
      size = 5
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.network-2.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.public-group.id]
  }
  metadata = {
    #user-data = "${file("./userkey")}"
    ssh-keys = "ubuntu:${file("/home/kit/Learning/terraform/keys/ssh_key.pub")}"
  }
}

#Create Elasticsearch VM
resource "yandex_compute_instance" "vm-5" {
  name = "elastic"
  zone = "ru-central1-b"
  resources {
    cores  = 2
    memory = 2
  }
  boot_disk {
    initialize_params {
      image_id = "fd8fte6bebi857ortlja"
      size = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.privnet-2.id
    nat       = false
    security_group_ids = [yandex_vpc_security_group.private-group.id]
  }
  metadata = {
    #user-data = "${file("./userkey")}"
    ssh-keys = "ubuntu:${file("/home/kit/Learning/terraform/keys/ssh_key.pub")}"
  }
}

#Create Kibana VM
resource "yandex_compute_instance" "vm-6" {
  name = "kibana"
  zone = "ru-central1-b"
  resources {
    cores  = 2
    memory = 2
  }
  boot_disk {
    initialize_params {
      image_id = "fd8fte6bebi857ortlja"
      size = 7
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.network-2.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.public-group.id]
  }
  metadata = {
    #user-data = "${file("./userkey")}"
    ssh-keys = "ubuntu:${file("/home/kit/Learning/terraform/keys/ssh_key.pub")}"
  }
}

#Create Bastion VM
resource "yandex_compute_instance" "vm-7" {
  name = "bastion"
  zone = "ru-central1-a"
  resources {
    cores  = 2
    memory = 2
  }
  boot_disk {
    initialize_params {
      image_id = "fd8fte6bebi857ortlja"
      size = 5
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.network-4.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.public-group.id]
  }
  metadata = {
    #user-data = "${file("./userkey")}"
    ssh-keys = "ubuntu:${file("/home/kit/Learning/terraform/keys/ssh_key.pub")}"
  }
}


# Create Target group
resource "yandex_alb_target_group" "web-target-group" {
  name           = "web-target-group"

  target {
    subnet_id    = yandex_vpc_subnet.privnet-1.id
    ip_address   = yandex_compute_instance.vm-1.network_interface.0.ip_address
  }

  target {
    subnet_id    = yandex_vpc_subnet.privnet-2.id
    ip_address   = yandex_compute_instance.vm-2.network_interface.0.ip_address
  }
}


#Create inventory
 resource "local_file" "ansible_inventory" {
  content = templatefile("./ansible/invtemplate.tmpl",
    {
     web1 = yandex_compute_instance.vm-1.network_interface.0.ip_address
     web2 = yandex_compute_instance.vm-2.network_interface.0.ip_address
     web1_ip = yandex_compute_instance.vm-1.network_interface.0.ip_address
     web2_ip = yandex_compute_instance.vm-2.network_interface.0.ip_address
     prometheus = yandex_compute_instance.vm-3.network_interface.0.ip_address
     prometheus_ip = yandex_compute_instance.vm-3.network_interface.0.ip_address
     grafana = yandex_compute_instance.vm-4.network_interface.0.nat_ip_address
     elastic = yandex_compute_instance.vm-5.network_interface.0.ip_address
     elastic_ip = yandex_compute_instance.vm-5.network_interface.0.ip_address
     kibana = yandex_compute_instance.vm-6.network_interface.0.nat_ip_address
     bastion = yandex_compute_instance.vm-7.network_interface.0.nat_ip_address
    }
  )
  filename = "./ansible/inventory"
}

# Create backend group
resource "yandex_alb_backend_group" "web-backend-group" {
  name                     = "web-backend-group"

  http_backend {
    name                   = "web-backend"
    weight                 = 1
    port                   = 80
    target_group_ids       = ["${yandex_alb_target_group.web-target-group.id}"]
    load_balancing_config {
      panic_threshold      = 90
    }    
    healthcheck {
      timeout              = "10s"
      interval             = "2s"
      healthy_threshold    = 10
      unhealthy_threshold  = 15 
      http_healthcheck {
        path               = "/"
      }
    }
  }
}

#Create HTTP Router
resource "yandex_alb_http_router" "tf-router" {
  name   = "tf-router"
  labels = {
    tf-label    = "tf-label-router"
    empty-label = ""
  }
}

resource "yandex_alb_virtual_host" "my-virtual-host" {
  name           = "my-virtual-host"
  http_router_id = "${yandex_alb_http_router.tf-router.id}"
  route {
    name = "main"
    http_route {
      http_route_action {
        backend_group_id = "${yandex_alb_backend_group.web-backend-group.id}"
        timeout          = "3s"
      }
    }
  }
}

#Create L7 load balancer
resource "yandex_alb_load_balancer" "test-balancer" {
  name        = "test-balancer"
  network_id  = yandex_vpc_network.network-1.id

  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
      subnet_id = yandex_vpc_subnet.network-3.id 
    }
  }

  listener {
    name = "web"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [ 80 ]
    }
    http {
      handler {
        http_router_id = yandex_alb_http_router.tf-router.id
      }
    }
  }
}


